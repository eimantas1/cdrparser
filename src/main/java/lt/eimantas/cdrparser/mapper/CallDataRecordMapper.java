package lt.eimantas.cdrparser.mapper;

import lt.eimantas.cdrparser.entities.dto.CallDataRecordAPI;
import lt.eimantas.cdrparser.entities.persistence.CallDataRecord;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Component
public class CallDataRecordMapper {

    public CallDataRecordAPI callDataRecordToApi(CallDataRecord callDataRecordDb){
        return new CallDataRecordAPI(callDataRecordDb.getRecordId(),
                callDataRecordDb.getCallingParty(), callDataRecordDb.getCalledParty(),
                callDataRecordDb.getStartTime(), callDataRecordDb.getEndTime(),
                callDataRecordDb.getStatus(), callDataRecordDb.getCost());
    }

    public List<CallDataRecordAPI> callDataRecordListToApi(List<CallDataRecord> callDataRecordDbList){
        return callDataRecordDbList
                .stream()
                .filter(Objects::nonNull)
                .map(this::callDataRecordToApi)
                .collect(Collectors.toList());
    }

    public CallDataRecord callDataRecordApiToDb(CallDataRecordAPI callDataRecordAPI){
        CallDataRecord callDataRecord = new CallDataRecord();
        callDataRecord.setRecordId(callDataRecordAPI.getRecordId());
        callDataRecord.setCallingParty(callDataRecordAPI.getCallingParty());
        callDataRecord.setCalledParty(callDataRecordAPI.getCalledParty());
        callDataRecord.setStartTime(callDataRecordAPI.getStartTime());
        callDataRecord.setEndTime(callDataRecordAPI.getEndTime());
        callDataRecord.setStatus(callDataRecordAPI.getStatus());
        callDataRecord.setCost(callDataRecordAPI.getCost());
        return  callDataRecord;
    }

    public List<CallDataRecord> callDataRecordApiListToDb(List<CallDataRecordAPI> callDataRecApiList){
        return callDataRecApiList
                .stream()
                .filter(Objects::nonNull)
                .map(this::callDataRecordApiToDb)
                .collect(Collectors.toList());
    }
}
