package lt.eimantas.cdrparser.persistence;

import lt.eimantas.cdrparser.entities.dto.Status;
import lt.eimantas.cdrparser.entities.persistence.CallDataRecord;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;


@Repository
public interface CallDataRecordRepository extends JpaRepository<CallDataRecord, Integer>{
    List<CallDataRecord> findByCalledParty(String calledParty);
    List<CallDataRecord> findByCallingParty(String callingParty);
    List<CallDataRecord> findByStartTimeAfterAndEndTimeBefore(LocalDateTime startTime, LocalDateTime endTime);
    List<CallDataRecord> findByCallingPartyAndStartTimeAfterAndEndTimeBefore(String callingParty,
                                                                             LocalDateTime startTime,
                                                                             LocalDateTime endTime);
    List<CallDataRecord> findByCalledPartyAndStartTimeAfterAndEndTimeBefore(String calledParty,
                                                                             LocalDateTime startTime,
                                                                             LocalDateTime endTime);

    List<CallDataRecord> findByStatus(Status status);
    Long countByCallingPartyAndStartTimeAfterAndEndTimeBefore(String callingParty,
                                                              LocalDateTime startTime,
                                                              LocalDateTime endTime);

}
