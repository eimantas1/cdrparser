package lt.eimantas.cdrparser.service;

import lt.eimantas.cdrparser.entities.dto.CallDataRecordAPI;
import lt.eimantas.cdrparser.entities.dto.CdrRequest;
import lt.eimantas.cdrparser.entities.dto.CdrRequestType;
import lt.eimantas.cdrparser.entities.dto.CdrResponse;
import lt.eimantas.cdrparser.entities.dto.Status;
import lt.eimantas.cdrparser.entities.persistence.CallDataRecord;
import lt.eimantas.cdrparser.mapper.CallDataRecordMapper;
import lt.eimantas.cdrparser.persistence.CallDataRecordRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.time.LocalDateTime;
import java.util.List;

import static lt.eimantas.cdrparser.entities.dto.CdrResponse.createSuccessResponse;

@Service
public class CdrServiceImpl implements CdrService {

    @Autowired
    private CallDataRecordRepository callDataRecordRepository;

    @Autowired
    private CallDataRecordMapper callDataRecordMapper;

    public CdrResponse listAll(CdrRequest request) {
        if (request.getCdrRequestType() != CdrRequestType.ALL_CALLS) {
            return CdrResponse.createIncorrectRequestResponse();
        }

        CdrResponse response = createSuccessResponse();
        List<CallDataRecordAPI> records = getAllCdrRecords();
        response.getCdrResponseBody().getRecordList().addAll(records);
        return response;
    }

    List<CallDataRecordAPI> getAllCdrRecords() {
        List<CallDataRecord> records = callDataRecordRepository.findAll();
        return callDataRecordMapper.callDataRecordListToApi(records);
    }

    public CdrResponse getCallsByCaller(CdrRequest request) {
        if (request.getCdrRequestType() != CdrRequestType.CALL_BY_USER) {
            return CdrResponse.createIncorrectRequestResponse();
        }
        String callingParty = request.getRequestBody().getCallingParty();
        CdrResponse response = createSuccessResponse();
        List<CallDataRecord> recordsDb = callDataRecordRepository.findByCallingParty(callingParty);
        List<CallDataRecordAPI> recordsApi = callDataRecordMapper.callDataRecordListToApi(recordsDb);
        response.getCdrResponseBody().getRecordList().addAll(recordsApi);
        return response;
    }

    public CdrResponse getCallsByCalled(CdrRequest request) {
        if (request.getCdrRequestType() != CdrRequestType.CALLS_BY_CALLED_USER) {
            return CdrResponse.createIncorrectRequestResponse();
        }
        String calledParty = request.getRequestBody().getCalledParty();
        CdrResponse response = createSuccessResponse();
        List<CallDataRecord> recordsDb = callDataRecordRepository.findByCalledParty(calledParty);
        List<CallDataRecordAPI> recordsApi = callDataRecordMapper.callDataRecordListToApi(recordsDb);
        response.getCdrResponseBody().getRecordList().addAll(recordsApi);
        return response;
    }

    public CdrResponse getCallsByPeriod(CdrRequest request) {
        if (request.getCdrRequestType() != CdrRequestType.CALLS_BY_PERIOD || request.getRequestBody() == null) {
            return CdrResponse.createIncorrectRequestResponse();
        }
        LocalDateTime startDate = request.getRequestBody().getStartDate();
        LocalDateTime endDate = request.getRequestBody().getEndDate();
        if (startDate == null || endDate == null) {
            return CdrResponse.createIncorrectRequestResponse();
        }

        CdrResponse response = createSuccessResponse();
        List<CallDataRecord> recordsDb = callDataRecordRepository.findByStartTimeAfterAndEndTimeBefore(startDate, endDate);
        List<CallDataRecordAPI> recordsApi = callDataRecordMapper.callDataRecordListToApi(recordsDb);
        response.getCdrResponseBody().getRecordList().addAll(recordsApi);
        return response;
    }

    @Override
    public CdrResponse getCallsByCallerAndPeriod(CdrRequest request) {
        if (request.getCdrRequestType() != CdrRequestType.CALLS_BY_CALLER_PERIOD || request.getRequestBody() == null) {
            return CdrResponse.createIncorrectRequestResponse();
        }
        LocalDateTime startDate = request.getRequestBody().getStartDate();
        LocalDateTime endDate = request.getRequestBody().getEndDate();
        String caller = request.getRequestBody().getCallingParty();
        if (areCallerAndDatesWrong(caller, startDate, endDate)) {
            return CdrResponse.createIncorrectRequestResponse();
        }

        CdrResponse response = createSuccessResponse();
        List<CallDataRecord> recordsDb = callDataRecordRepository.findByCallingPartyAndStartTimeAfterAndEndTimeBefore(caller, startDate, endDate);
        List<CallDataRecordAPI> recordsApi = callDataRecordMapper.callDataRecordListToApi(recordsDb);
        response.getCdrResponseBody().getRecordList().addAll(recordsApi);
        return response;
    }

    @Override
    public CdrResponse getCallsByCalledAndPeriod(CdrRequest request) {
        if (request.getCdrRequestType() != CdrRequestType.CALLS_BY_CALLED_PERIOD || request.getRequestBody() == null) {
            return CdrResponse.createIncorrectRequestResponse();
        }
        LocalDateTime startDate = request.getRequestBody().getStartDate();
        LocalDateTime endDate = request.getRequestBody().getEndDate();
        String called = request.getRequestBody().getCalledParty();
        if (areCallerAndDatesWrong(called, startDate, endDate)) {
            return CdrResponse.createIncorrectRequestResponse();
        }
        CdrResponse response = createSuccessResponse();
        List<CallDataRecord> recordsDb = callDataRecordRepository.findByCalledPartyAndStartTimeAfterAndEndTimeBefore(called, startDate, endDate);
        List<CallDataRecordAPI> recordsApi = callDataRecordMapper.callDataRecordListToApi(recordsDb);
        response.getCdrResponseBody().getRecordList().addAll(recordsApi);
        return response;
    }

    @Override
    public CdrResponse getCallsByStatus(CdrRequest request) {
        if (request.getCdrRequestType() != CdrRequestType.CALL_BY_STATUS || request.getRequestBody() == null) {
            return CdrResponse.createIncorrectRequestResponse();
        }
        Status status = request.getRequestBody().getStatus();
        if (status == null) {
            return CdrResponse.createIncorrectRequestResponse();
        }
        CdrResponse response = createSuccessResponse();
        List<CallDataRecord> recordsDb = callDataRecordRepository.findByStatus(status);
        List<CallDataRecordAPI> recordsApi = callDataRecordMapper.callDataRecordListToApi(recordsDb);
        response.getCdrResponseBody().getRecordList().addAll(recordsApi);
        return response;
    }

    @Override
    public CdrResponse getCountByCallerAndPeriod(CdrRequest request) {
        if (request.getCdrRequestType() != CdrRequestType.COUNT_BY_CALLER_PERIOD || request.getRequestBody() == null) {
            return CdrResponse.createIncorrectRequestResponse();
        }
        LocalDateTime startDate = request.getRequestBody().getStartDate();
        LocalDateTime endDate = request.getRequestBody().getEndDate();
        String caller = request.getRequestBody().getCallingParty();

        if (areCallerAndDatesWrong(caller, startDate, endDate)) {
            return CdrResponse.createIncorrectRequestResponse();
        }

        CdrResponse response = createSuccessResponse();
        Long count = callDataRecordRepository.countByCallingPartyAndStartTimeAfterAndEndTimeBefore(caller, startDate, endDate);
        response.getCdrResponseBody().setCount(count);
        return response;
    }

    private boolean areCallerAndDatesWrong(String caller, LocalDateTime startDate, LocalDateTime endDate) {
        return (startDate == null || endDate == null || StringUtils.isEmpty(caller));
    }
}
