package lt.eimantas.cdrparser.service;

import lt.eimantas.cdrparser.entities.dto.CallDataRecordAPI;
import lt.eimantas.cdrparser.entities.dto.CdrFields;
import lt.eimantas.cdrparser.entities.dto.CdrResponse;
import lt.eimantas.cdrparser.entities.dto.Status;
import lt.eimantas.cdrparser.entities.persistence.CallDataRecord;
import lt.eimantas.cdrparser.exception.CsvParsingException;
import lt.eimantas.cdrparser.mapper.CallDataRecordMapper;
import lt.eimantas.cdrparser.persistence.CallDataRecordRepository;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.TimeZone;

import static lt.eimantas.cdrparser.entities.dto.CdrResponse.FAILED_PARSE_CSV_FILE;

@Service
public class FileServiceImpl implements FileService {

    private final ZoneId utcZoneId = TimeZone.getTimeZone("UTC").toZoneId();

    @Autowired
    private CallDataRecordMapper mapper;

    @Autowired
    private CallDataRecordRepository repository;

    @Transactional
    public CdrResponse storeStreamToDatabase(InputStream inputStream) {
        try {
            Reader inputReader = new InputStreamReader(inputStream);
            Iterable<CSVRecord> records = CSVFormat.RFC4180.withFirstRecordAsHeader().parse(inputReader);
            for (CSVRecord record : records) {
                CallDataRecordAPI cdrRecord = parseCsvEntry(record);
                CallDataRecord dbRecord = mapper.callDataRecordApiToDb(cdrRecord);
                repository.save(dbRecord);
            }
            return CdrResponse.createSuccessResponse();
        } catch (IOException | CsvParsingException | IllegalArgumentException e) {
            return  CdrResponse.createFailedParseResponse();
        }
    }

    CallDataRecordAPI parseCsvEntry(CSVRecord record) throws CsvParsingException {
        String id = record.get(CdrFields.ID);
        String caller = record.get(CdrFields.ACCOUNT);
        String destination = record.get(CdrFields.DESTINATION);

        String startDateString = record.get(CdrFields.STARTDATE);
        LocalDateTime startDate = utcTimeStampToLocalDateTime(startDateString);

        String endDateString = record.get(CdrFields.ENDDATE);
        LocalDateTime endDate = utcTimeStampToLocalDateTime(endDateString);

        Status status = Status.get(record.get(CdrFields.STATUS));
        String cost = record.get(CdrFields.COSTPERMINUTE);

        return new CallDataRecordAPI(id, caller, destination, startDate,
                endDate, status, cost);
    }

    LocalDateTime utcTimeStampToLocalDateTime(String timeStampString) throws CsvParsingException {
        try {
            return LocalDateTime.ofInstant(Instant.ofEpochSecond(Long.parseLong(timeStampString)), utcZoneId);
        }
        catch(NumberFormatException nfe){
            throw new CsvParsingException(FAILED_PARSE_CSV_FILE);
        }
    }
}

