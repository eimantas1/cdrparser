package lt.eimantas.cdrparser.service;

import lt.eimantas.cdrparser.entities.dto.CdrResponse;

import java.io.InputStream;

public interface FileService {

    CdrResponse storeStreamToDatabase(InputStream stream);
}
