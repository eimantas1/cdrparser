package lt.eimantas.cdrparser.service;

import lt.eimantas.cdrparser.entities.dto.CdrRequest;
import lt.eimantas.cdrparser.entities.dto.CdrResponse;

public interface CdrService {
    CdrResponse listAll(CdrRequest request);
    CdrResponse getCallsByCaller(CdrRequest request);
    CdrResponse getCallsByCalled(CdrRequest request);
    CdrResponse getCallsByPeriod(CdrRequest request);
    CdrResponse getCallsByCallerAndPeriod(CdrRequest request);
    CdrResponse getCallsByCalledAndPeriod(CdrRequest request);
    CdrResponse getCallsByStatus(CdrRequest request);
    CdrResponse getCountByCallerAndPeriod(CdrRequest request);
}
