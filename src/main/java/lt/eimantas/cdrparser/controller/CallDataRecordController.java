package lt.eimantas.cdrparser.controller;

import lt.eimantas.cdrparser.entities.dto.CdrRequest;
import lt.eimantas.cdrparser.entities.dto.CdrResponse;
import lt.eimantas.cdrparser.service.CdrService;
import lt.eimantas.cdrparser.service.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

import static lt.eimantas.cdrparser.entities.dto.CdrResponse.createFailedParseResponse;

@RestController
@RequestMapping("/api")
public class CallDataRecordController {

    @Autowired
    private FileService fileService;

    @Autowired
    private CdrService cdrService;


    @PostMapping("/allRecords")
    public CdrResponse listAll(@RequestBody @NonNull CdrRequest request) {
        return cdrService.listAll(request);
    }

    @PostMapping("/getCallsByCaller")
    public CdrResponse getCallsByCaller(@RequestBody @NonNull CdrRequest request) {
        return cdrService.getCallsByCaller(request);
    }

    @PostMapping("/getCallsByCalled")
    public CdrResponse getCallsByCalled(@RequestBody @NonNull CdrRequest request) {
        return cdrService.getCallsByCalled(request);
    }

    @PostMapping("/getCallsByPeriod")
    public CdrResponse getCallsByPeriod(@RequestBody @NonNull CdrRequest request) {
        return cdrService.getCallsByPeriod(request);
    }

    @PostMapping("/getCallsByCallerAndPeriod")
    public CdrResponse getCallsByCallerAndPeriod(@RequestBody @NonNull CdrRequest request) {
        return cdrService.getCallsByCallerAndPeriod(request);
    }

    @PostMapping("/getCallsByCalledAndPeriod")
    public CdrResponse getCallsByCalledAndPeriod(@RequestBody @NonNull CdrRequest request) {
        return cdrService.getCallsByCalledAndPeriod(request);
    }

    @PostMapping("/getCallsByStatus")
    public CdrResponse getCallsByStatus(@RequestBody @NonNull CdrRequest request) {
        return cdrService.getCallsByStatus(request);
    }

    @PostMapping("/getCountByCallerAndPeriod")
    public CdrResponse getCountByCallerAndPeriod(@RequestBody @NonNull CdrRequest request) {
        return cdrService.getCountByCallerAndPeriod(request);
    }

    @PostMapping(value = "/upload")
    public CdrResponse handleFileUpload(@NonNull @RequestParam("file") MultipartFile file) {
        try {
            return fileService.storeStreamToDatabase(file.getInputStream());
        } catch (IOException e) {
            return createFailedParseResponse();
        }
    }
}
