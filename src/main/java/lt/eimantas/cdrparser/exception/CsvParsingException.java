package lt.eimantas.cdrparser.exception;

public class CsvParsingException extends  Exception {

    public CsvParsingException(String message) {
        super(message);
    }
}
