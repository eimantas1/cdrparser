package lt.eimantas.cdrparser.entities.persistence;

import lombok.Data;
import lombok.NoArgsConstructor;
import lt.eimantas.cdrparser.entities.dto.Status;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.LocalDateTime;

@Data
@Entity
@NoArgsConstructor
public class CallDataRecord {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String recordId;
    private String callingParty;
    private String calledParty;
    private LocalDateTime startTime;
    private LocalDateTime endTime;
    private Status status;
    private String cost;
}
