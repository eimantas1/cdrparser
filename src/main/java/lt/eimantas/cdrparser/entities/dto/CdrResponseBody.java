package lt.eimantas.cdrparser.entities.dto;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class CdrResponseBody {
    private List<CallDataRecordAPI> recordList = new ArrayList<>();
    private Long count;
}
