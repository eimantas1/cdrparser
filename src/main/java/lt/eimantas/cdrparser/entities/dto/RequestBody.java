package lt.eimantas.cdrparser.entities.dto;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class RequestBody {
    private String callingParty;
    private String calledParty;
    private LocalDateTime startDate;
    private LocalDateTime endDate;
    private Status status;
}
