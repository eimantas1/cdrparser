package lt.eimantas.cdrparser.entities.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class CdrRequest {
    private final String apiVersion;
    private final CdrRequestType cdrRequestType;
    private final RequestBody requestBody;
}
