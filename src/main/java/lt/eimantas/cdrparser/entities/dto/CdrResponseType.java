package lt.eimantas.cdrparser.entities.dto;

public enum CdrResponseType {
    INCORRECT_REQUEST, SUCCESS, FAILURE
}
