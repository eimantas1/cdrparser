package lt.eimantas.cdrparser.entities.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

import static lt.eimantas.cdrparser.entities.dto.CdrResponseType.FAILURE;
import static lt.eimantas.cdrparser.entities.dto.CdrResponseType.INCORRECT_REQUEST;
import static lt.eimantas.cdrparser.entities.dto.CdrResponseType.SUCCESS;

@AllArgsConstructor
@Getter
public class CdrResponse {

    public static final String INCORRECT_REQUEST_MSG = "Incorrect request type for endpoint.";
    public static final String EMPTY_MSG = "";
    public static final String FAILED_PARSE_CSV_FILE = "Failed to parse CSV file.";

    private final CdrResponseType cdrResponseType;
    private final CdrResponseBody cdrResponseBody;
    private final String message;

    public CdrResponse(CdrResponseType type, String msg) {
        cdrResponseType = type;
        message = msg;
        cdrResponseBody = new CdrResponseBody();
    }

    public static CdrResponse createIncorrectRequestResponse() {
        return new CdrResponse(INCORRECT_REQUEST, INCORRECT_REQUEST_MSG);
    }

    public static CdrResponse createSuccessResponse() {
        return new CdrResponse(SUCCESS, EMPTY_MSG);
    }

    public static CdrResponse createFailedParseResponse(){
        return new CdrResponse(FAILURE, FAILED_PARSE_CSV_FILE);
    }
}
