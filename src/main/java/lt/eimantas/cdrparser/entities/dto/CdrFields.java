package lt.eimantas.cdrparser.entities.dto;

public enum CdrFields {
    ID, ACCOUNT, DESTINATION, STARTDATE, ENDDATE, STATUS, COSTPERMINUTE
}
