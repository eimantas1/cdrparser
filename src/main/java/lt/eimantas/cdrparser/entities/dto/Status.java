package lt.eimantas.cdrparser.entities.dto;

import java.util.HashMap;
import java.util.Map;

public enum Status {
    SUCCESS("success"),
    FAILED("failed");

    private final String caption;

    private static final Map<String, Status> lookup = new HashMap<>();

    Status(String caption) {
        this.caption = caption;
    }

    public String getCaption() {
        return caption;
    }

    static
    {
        for(Status env : Status.values())
        {
            lookup.put(env.getCaption(), env);
        }
    }

    public static Status get(String caption)
    {
        return lookup.get(caption);
    }
}
