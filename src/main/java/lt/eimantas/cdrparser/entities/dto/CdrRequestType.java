package lt.eimantas.cdrparser.entities.dto;

public enum CdrRequestType {
    ALL_CALLS,
    CALL_BY_USER,
    CALLS_BY_CALLED_USER,
    CALLS_BY_PERIOD,
    CALLS_BY_CALLER_PERIOD,
    CALLS_BY_CALLED_PERIOD,
    CALL_BY_STATUS,
    COUNT_BY_CALLER_PERIOD
}
