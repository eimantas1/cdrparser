package lt.eimantas.cdrparser.persistence;

import lt.eimantas.cdrparser.entities.dto.Status;
import lt.eimantas.cdrparser.entities.persistence.CallDataRecord;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.CollectionUtils;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import static lt.eimantas.cdrparser.utils.CallDataRecordTestUtil.createFebruary16;
import static lt.eimantas.cdrparser.utils.CallDataRecordTestUtil.createFebruary16failed;
import static lt.eimantas.cdrparser.utils.CallDataRecordTestUtil.createMarch11;
import static lt.eimantas.cdrparser.utils.CallDataRecordTestUtil.createMarch11WithCaller;
import static lt.eimantas.cdrparser.utils.CallerUtils.getCaller1;
import static lt.eimantas.cdrparser.utils.CallerUtils.getCaller2;
import static lt.eimantas.cdrparser.utils.CallerUtils.getCaller3;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

@RunWith(SpringRunner.class)
@DataJpaTest
public class CallDataRecordRepositoryTest {

    @Autowired
    TestEntityManager entityManager;

    @Autowired
    CallDataRecordRepository repo;

    @Test
    public void testSaveFind() {
        CallDataRecord march11 = new CallDataRecord();
        march11.setStartTime(LocalDateTime.of(2020, 3, 11, 10, 30, 20));
        march11.setEndTime(LocalDateTime.of(2020, 3, 11, 10, 32, 28));
        march11 = entityManager.persistAndFlush(march11);

        Optional<CallDataRecord> march11Optional = repo.findById(march11.getId());
        assertTrue(march11Optional.isPresent());
        CallDataRecord march11db = march11Optional.get();
        assertEquals(march11db, march11);
    }

    @Test
    public void testFindPeriod() {
        CallDataRecord march11 = createMarch11();
        march11 = entityManager.persistAndFlush(march11);

        CallDataRecord february16 = createFebruary16();
        entityManager.persistAndFlush(february16);

        LocalDateTime march1st = LocalDateTime.of(2020, 3, 1, 0, 0, 0);
        LocalDateTime march15th = LocalDateTime.of(2020, 3, 15, 0, 0, 0);
        LocalDateTime march31st = LocalDateTime.of(2020, 3, 31, 23, 59, 59);

        List<CallDataRecord> marchRecords = repo.findByStartTimeAfterAndEndTimeBefore(march1st, march31st);
        assertFalse(CollectionUtils.isEmpty(marchRecords));
        assertEquals(1, marchRecords.size());
        assertEquals(march11, marchRecords.get(0));

        marchRecords = repo.findByStartTimeAfterAndEndTimeBefore(march15th, march31st);
        assertTrue(CollectionUtils.isEmpty(marchRecords));
    }

    @Test
    public void testFindCallingParty() {
        CallDataRecord march11 = createMarch11();
        march11 = entityManager.persistAndFlush(march11);

        CallDataRecord february16 = createFebruary16();
        entityManager.persistAndFlush(february16);

        List<CallDataRecord> callRecords = repo.findByCallingParty(getCaller1());
        assertFalse(CollectionUtils.isEmpty(callRecords));
        assertEquals(2, callRecords.size());
        assertEquals(march11, callRecords.get(0));
        callRecords = repo.findByCallingParty(getCaller2());
        assertTrue(CollectionUtils.isEmpty(callRecords));
    }

    @Test
    public void testCallerFindPeriod() {
        CallDataRecord march11 = createMarch11();
        march11 = entityManager.persistAndFlush(march11);

        CallDataRecord february16 = createFebruary16();
        entityManager.persistAndFlush(february16);

        LocalDateTime march1st = LocalDateTime.of(2020, 3, 1, 0, 0, 0);
        LocalDateTime march31st = LocalDateTime.of(2020, 3, 31, 23, 59, 59);

        String caller1 = getCaller1();
        List<CallDataRecord> marchRecords = repo.findByCallingPartyAndStartTimeAfterAndEndTimeBefore(caller1, march1st, march31st);

        assertFalse(CollectionUtils.isEmpty(marchRecords));
        assertEquals(1, marchRecords.size());
        assertEquals(march11, marchRecords.get(0));

        marchRecords = repo.findByCallingPartyAndStartTimeAfterAndEndTimeBefore(getCaller2(), march1st, march31st);
        assertTrue(CollectionUtils.isEmpty(marchRecords));
    }


    @Test
    public void countByCallerDuringPeriodTest() {
        entityManager.persistAndFlush(createMarch11WithCaller(getCaller1()));
        entityManager.persistAndFlush(createMarch11WithCaller(getCaller2()));
        LocalDateTime march1st = LocalDateTime.of(2020, 3, 1, 0, 0, 0);
        LocalDateTime march31st = LocalDateTime.of(2020, 3, 31, 23, 59, 59);

        String caller1 = getCaller1();
        Long marchCallCount = repo.countByCallingPartyAndStartTimeAfterAndEndTimeBefore(caller1, march1st, march31st);
        assertEquals(1, (long) marchCallCount);
        marchCallCount = repo.countByCallingPartyAndStartTimeAfterAndEndTimeBefore(getCaller2(), march1st, march31st);
        assertEquals(1, (long) marchCallCount);
        marchCallCount = repo.countByCallingPartyAndStartTimeAfterAndEndTimeBefore(getCaller3(), march1st, march31st);
        assertEquals(0, (long) marchCallCount);

        LocalDateTime march15th = LocalDateTime.of(2020, 3, 15, 0, 0, 0);
        marchCallCount = repo.countByCallingPartyAndStartTimeAfterAndEndTimeBefore(getCaller1(), march15th, march31st);
        assertEquals(0, (long) marchCallCount);
    }

    @Test
    public void findByStatusTest(){
        CallDataRecord march11 = createMarch11();
        entityManager.persistAndFlush(march11);

        CallDataRecord february16 = createFebruary16();
        entityManager.persistAndFlush(february16);

        CallDataRecord february16failed = createFebruary16failed();
        entityManager.persistAndFlush(february16failed);

        List<CallDataRecord> records = repo.findByStatus(Status.SUCCESS);
        assertEquals(2, records.size());
        records = repo.findByStatus(Status.FAILED);
        assertEquals(1, records.size());
    }

}
