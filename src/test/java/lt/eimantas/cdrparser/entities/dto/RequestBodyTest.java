package lt.eimantas.cdrparser.entities.dto;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.junit.Test;

import static lt.eimantas.cdrparser.entities.dto.Status.SUCCESS;
import static org.junit.Assert.assertEquals;

public class RequestBodyTest {

    @Test
    public void requestBodyParsingTest() throws JsonProcessingException {
        String requestBody = "{\"callingParty\":\"123\",\"calledParty\":\"321\",\"startDate\":\"2020-03-11T10:30:20\",\"endDate\":\"2020-03-11T14:30:20\",\"status\":\"SUCCESS\"}";

        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());
        mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        RequestBody result = mapper.readValue(requestBody, RequestBody.class);
        assertEquals(SUCCESS, result.getStatus());

    }

}