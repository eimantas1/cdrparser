package lt.eimantas.cdrparser.utils;

import lt.eimantas.cdrparser.entities.dto.CdrRequest;
import lt.eimantas.cdrparser.entities.dto.RequestBody;
import lt.eimantas.cdrparser.entities.dto.Status;

import static lt.eimantas.cdrparser.entities.dto.CdrRequestType.ALL_CALLS;
import static lt.eimantas.cdrparser.entities.dto.CdrRequestType.CALLS_BY_CALLED_PERIOD;
import static lt.eimantas.cdrparser.entities.dto.CdrRequestType.CALLS_BY_CALLED_USER;
import static lt.eimantas.cdrparser.entities.dto.CdrRequestType.CALLS_BY_CALLER_PERIOD;
import static lt.eimantas.cdrparser.entities.dto.CdrRequestType.CALLS_BY_PERIOD;
import static lt.eimantas.cdrparser.entities.dto.CdrRequestType.CALL_BY_STATUS;
import static lt.eimantas.cdrparser.entities.dto.CdrRequestType.CALL_BY_USER;
import static lt.eimantas.cdrparser.utils.CallerUtils.getCaller1;

public class CdrRequestTestUtil {

    private static final String version = "1.0";

    public static CdrRequest createListAllRequest() {
        RequestBody requestBody = new RequestBody();
        return new CdrRequest(version, ALL_CALLS, requestBody);
    }

    public static CdrRequest createListByCallerRequest() {
        RequestBody requestBody = new RequestBody();
        requestBody.setCallingParty(getCaller1());
        return new CdrRequest(version, CALL_BY_USER, requestBody);
    }

    public static CdrRequest createListByCalledRequest() {
        RequestBody requestBody = new RequestBody();
        requestBody.setCalledParty(getCaller1());
        return new CdrRequest(version, CALLS_BY_CALLED_USER, requestBody);
    }

    public static CdrRequest createListByStatusRequest() {
        RequestBody requestBody = new RequestBody();
        requestBody.setStatus(Status.SUCCESS);
        return new CdrRequest(version, CALL_BY_STATUS, requestBody);
    }

    public static CdrRequest createListByCallerPeriodRequest() {
        RequestBody requestBody = new RequestBody();
        requestBody.setCallingParty(getCaller1());
        return new CdrRequest(version, CALLS_BY_CALLER_PERIOD, requestBody);
    }

    public static CdrRequest createListByCalledPeriodRequest() {
        RequestBody requestBody = new RequestBody();
        requestBody.setCalledParty(getCaller1());
        return new CdrRequest(version, CALLS_BY_CALLED_PERIOD, requestBody);
    }

    public static CdrRequest createListPeriodRequest() {
        RequestBody requestBody = new RequestBody();
        requestBody.setCallingParty(getCaller1());
        return new CdrRequest(version, CALLS_BY_PERIOD, requestBody);
    }
}
