package lt.eimantas.cdrparser.utils;

public class CallerUtils {

    public static String getCaller1() {
        return "+37061234567";
    }

    public static String getCaller2() {
        return "+37061234568";
    }

    public static String getCaller3() {
        return "+37061234569";
    }
}
