package lt.eimantas.cdrparser.utils;

import java.time.LocalDateTime;

public class LocalDateTimeUtils {

    public static LocalDateTime march11Morning(){
        return LocalDateTime.of(2020, 3, 11, 10, 30, 20);
    }

    public static LocalDateTime march11Afternoon(){
        return LocalDateTime.of(2020, 3, 11, 14, 30, 20);
    }

    public static LocalDateTime february16Morning(){
        return LocalDateTime.of(2020, 2, 16, 10, 30, 20);
    }

    public static LocalDateTime february16Afternoon(){
        return LocalDateTime.of(2020, 2, 16, 14, 30, 20);
    }
}
