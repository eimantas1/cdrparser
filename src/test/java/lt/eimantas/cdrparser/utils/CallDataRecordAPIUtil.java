package lt.eimantas.cdrparser.utils;

import lt.eimantas.cdrparser.entities.dto.CallDataRecordAPI;
import lt.eimantas.cdrparser.entities.dto.Status;

import java.util.UUID;

import static lt.eimantas.cdrparser.utils.CallerUtils.getCaller1;
import static lt.eimantas.cdrparser.utils.CallerUtils.getCaller2;
import static lt.eimantas.cdrparser.utils.LocalDateTimeUtils.march11Afternoon;
import static lt.eimantas.cdrparser.utils.LocalDateTimeUtils.march11Morning;

public class CallDataRecordAPIUtil {
    public static CallDataRecordAPI createApiRecord() {
        return new CallDataRecordAPI(UUID.randomUUID().toString(),
                getCaller1(),
                getCaller2(),
                march11Morning(),
                march11Afternoon(),
                Status.SUCCESS,
                "0.5");
    }
}
