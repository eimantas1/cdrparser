package lt.eimantas.cdrparser.utils;

import lt.eimantas.cdrparser.entities.dto.Status;
import lt.eimantas.cdrparser.entities.persistence.CallDataRecord;

import java.util.UUID;

import static lt.eimantas.cdrparser.utils.CallerUtils.getCaller1;
import static lt.eimantas.cdrparser.utils.CallerUtils.getCaller2;
import static lt.eimantas.cdrparser.utils.LocalDateTimeUtils.february16Afternoon;
import static lt.eimantas.cdrparser.utils.LocalDateTimeUtils.february16Morning;
import static lt.eimantas.cdrparser.utils.LocalDateTimeUtils.march11Afternoon;
import static lt.eimantas.cdrparser.utils.LocalDateTimeUtils.march11Morning;

public class CallDataRecordTestUtil {

    public static CallDataRecord createMarch11() {
        CallDataRecord march11 = new CallDataRecord();
        march11.setStartTime(march11Morning());
        march11.setEndTime(march11Afternoon());
        march11.setCallingParty(getCaller1());
        march11.setCalledParty(getCaller2());
        march11.setCost("0.1");
        march11.setRecordId(UUID.randomUUID().toString());
        march11.setStatus(Status.SUCCESS);
        return march11;
    }

    public static CallDataRecord createMarch11WithCaller(String caller) {
        CallDataRecord march11 = new CallDataRecord();
        march11.setStartTime(march11Morning());
        march11.setEndTime(march11Afternoon());
        march11.setCallingParty(caller);
        march11.setCalledParty(getCaller2());
        march11.setCost("0.1");
        march11.setRecordId(UUID.randomUUID().toString());
        march11.setStatus(Status.SUCCESS);
        return march11;
    }


    public static CallDataRecord createFebruary16() {
        CallDataRecord march11 = new CallDataRecord();
        march11.setStartTime(february16Morning());
        march11.setEndTime(february16Afternoon());
        march11.setCallingParty(getCaller1());
        march11.setCalledParty(getCaller2());
        march11.setCost("0.1");
        march11.setRecordId(UUID.randomUUID().toString());
        march11.setStatus(Status.SUCCESS);
        return march11;
    }

    public static CallDataRecord createFebruary16failed() {
        CallDataRecord march11 = new CallDataRecord();
        march11.setStartTime(february16Morning());
        march11.setEndTime(february16Afternoon());
        march11.setCallingParty(getCaller1());
        march11.setCalledParty(getCaller2());
        march11.setCost("0.1");
        march11.setRecordId(UUID.randomUUID().toString());
        march11.setStatus(Status.FAILED);
        return march11;
    }
}
