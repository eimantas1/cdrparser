package lt.eimantas.cdrparser.service;

import lt.eimantas.cdrparser.entities.dto.CdrResponse;
import lt.eimantas.cdrparser.entities.persistence.CallDataRecord;
import lt.eimantas.cdrparser.mapper.CallDataRecordMapper;
import lt.eimantas.cdrparser.persistence.CallDataRecordRepository;
import lt.eimantas.cdrparser.utils.CallDataRecordTestUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.InputStream;

import static lt.eimantas.cdrparser.entities.dto.CdrResponse.FAILED_PARSE_CSV_FILE;
import static lt.eimantas.cdrparser.entities.dto.CdrResponseType.FAILURE;
import static lt.eimantas.cdrparser.entities.dto.CdrResponseType.SUCCESS;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class FileServiceImplTest {


    @InjectMocks
    private final FileService fileService = new FileServiceImpl();

    @Mock
    private CallDataRecordRepository repo;

    @Mock
    private CallDataRecordMapper callDataRecordMapper;


    @Test
    public void csvParsingTest() {
        CallDataRecord recordDb = CallDataRecordTestUtil.createMarch11();
        when(repo.save(any())).thenReturn(recordDb);

        InputStream inputStream = getClass().getClassLoader().getResourceAsStream("cdrtest.csv");
        CdrResponse response = fileService.storeStreamToDatabase(inputStream);
        assertSame(SUCCESS, response.getCdrResponseType());
    }

    @Test
    public void badCsvParsingTest() {
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream("wrongcsv.csv");
        CdrResponse response = fileService.storeStreamToDatabase(inputStream);
        assertSame(FAILURE, response.getCdrResponseType());
        assertEquals(FAILED_PARSE_CSV_FILE, response.getMessage());
    }

    @Test
    public void notCsvParsingTest() {
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream("totallyWrong.csv");
        CdrResponse response = fileService.storeStreamToDatabase(inputStream);
        assertSame(FAILURE, response.getCdrResponseType());
        assertEquals(FAILED_PARSE_CSV_FILE, response.getMessage());
    }
}