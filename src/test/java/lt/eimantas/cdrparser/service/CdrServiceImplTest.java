package lt.eimantas.cdrparser.service;

import lt.eimantas.cdrparser.entities.dto.CallDataRecordAPI;
import lt.eimantas.cdrparser.entities.dto.CdrRequest;
import lt.eimantas.cdrparser.entities.dto.CdrResponse;
import lt.eimantas.cdrparser.entities.dto.CdrResponseType;
import lt.eimantas.cdrparser.entities.dto.Status;
import lt.eimantas.cdrparser.entities.persistence.CallDataRecord;
import lt.eimantas.cdrparser.mapper.CallDataRecordMapper;
import lt.eimantas.cdrparser.persistence.CallDataRecordRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Collections;

import static lt.eimantas.cdrparser.utils.CallDataRecordAPIUtil.createApiRecord;
import static lt.eimantas.cdrparser.utils.CallDataRecordTestUtil.createMarch11;
import static lt.eimantas.cdrparser.utils.CallerUtils.getCaller1;
import static lt.eimantas.cdrparser.utils.CdrRequestTestUtil.createListAllRequest;
import static lt.eimantas.cdrparser.utils.CdrRequestTestUtil.createListByCalledPeriodRequest;
import static lt.eimantas.cdrparser.utils.CdrRequestTestUtil.createListByCalledRequest;
import static lt.eimantas.cdrparser.utils.CdrRequestTestUtil.createListByCallerPeriodRequest;
import static lt.eimantas.cdrparser.utils.CdrRequestTestUtil.createListByCallerRequest;
import static lt.eimantas.cdrparser.utils.CdrRequestTestUtil.createListByStatusRequest;
import static lt.eimantas.cdrparser.utils.CdrRequestTestUtil.createListPeriodRequest;
import static lt.eimantas.cdrparser.utils.LocalDateTimeUtils.march11Afternoon;
import static lt.eimantas.cdrparser.utils.LocalDateTimeUtils.march11Morning;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CdrServiceImplTest
{
    @InjectMocks
    private final CdrService cdrService = new CdrServiceImpl();

    @Mock
    private CallDataRecordRepository repo;

    @Mock
    private CallDataRecordMapper callDataRecordMapper;

    @Test
    public void searchAllTest(){
        CallDataRecord cdr = createMarch11();
        when(repo.findAll()).thenReturn(Collections.singletonList(cdr));
        CallDataRecordAPI api = createApiRecord();
        CdrRequest request = createListAllRequest();
        cdrService.listAll(request);
        verify(repo,times(1)).findAll();
    }

    @Test
    public void findByCallerTest(){
        String caller = getCaller1();
        CallDataRecord cdr = createMarch11();
        when(repo.findByCallingParty(caller)).thenReturn(Collections.singletonList(cdr));
        CdrRequest request = createListByCallerRequest();
        CdrResponse response = cdrService.getCallsByCaller(request);
        verify(repo,times(0)).findAll();
        verify(repo,times(1)).findByCallingParty(caller);
        assertEquals(CdrResponseType.SUCCESS, response.getCdrResponseType());
    }

    @Test
    public void findByStatusTest(){
        String caller = getCaller1();
        CallDataRecord cdr = createMarch11();
        when(repo.findByStatus(any())).thenReturn(Collections.singletonList(cdr));
        CdrRequest request = createListByStatusRequest();
        CdrResponse response = cdrService.getCallsByStatus(request);
        verify(repo,times(0)).findAll();
        verify(repo,times(0)).findByCallingParty(caller);
        verify(repo,times(1)).findByStatus(Status.SUCCESS);
        verify(repo,times(0)).findByStatus(Status.FAILED);
        assertEquals(CdrResponseType.SUCCESS, response.getCdrResponseType());
    }

    @Test
    public void findByCalledTest(){
        String called = getCaller1();
        CallDataRecord cdr = createMarch11();
        when(repo.findByCalledParty(called)).thenReturn(Collections.singletonList(cdr));
        CdrRequest request = createListByCalledRequest();
        CdrResponse response = cdrService.getCallsByCalled(request);
        verify(repo,times(0)).findAll();
        verify(repo,times(0)).findByCallingParty(called);
        verify(repo,times(1)).findByCalledParty(called);
        assertEquals(CdrResponseType.SUCCESS, response.getCdrResponseType());
    }

    @Test
    public void findByCallerAndIncorrectDatesTest(){
        CdrRequest request = createListByCallerPeriodRequest();
        CdrResponse response = cdrService.getCallsByCallerAndPeriod(request);
        assertEquals(CdrResponseType.INCORRECT_REQUEST, response.getCdrResponseType());
    }

    @Test
    public void findByCallerAndCorrectDatesTest(){
        CdrRequest request = createListByCallerPeriodRequest();
        request.getRequestBody().setStartDate(march11Morning());
        request.getRequestBody().setEndDate(march11Afternoon());
        CdrResponse response = cdrService.getCallsByCallerAndPeriod(request);
        verify(repo,times(1)).findByCallingPartyAndStartTimeAfterAndEndTimeBefore(any(), any(), any());
        verify(repo,times(0)).findByCalledPartyAndStartTimeAfterAndEndTimeBefore(any(), any(), any());
        assertEquals(CdrResponseType.SUCCESS, response.getCdrResponseType());
    }

    @Test
    public void getCallsByPeriod() {
        CdrRequest request = createListPeriodRequest();
        request.getRequestBody().setStartDate(march11Morning());
        request.getRequestBody().setEndDate(march11Afternoon());
        CdrResponse response = cdrService.getCallsByPeriod(request);
        verify(repo,times(1)).findByStartTimeAfterAndEndTimeBefore(any(), any());
        assertEquals(CdrResponseType.SUCCESS, response.getCdrResponseType());
    }

    @Test
    public void getCallsByPeriodMissingDate() {
        CdrRequest request = createListPeriodRequest();
        request.getRequestBody().setEndDate(march11Afternoon());
        CdrResponse response = cdrService.getCallsByPeriod(request);
        verify(repo,times(0)).findByStartTimeAfterAndEndTimeBefore(any(), any());
        assertEquals(CdrResponseType.INCORRECT_REQUEST, response.getCdrResponseType());
    }

    @Test
    public void getCalledByPeriod() {
        CdrRequest request = createListByCalledPeriodRequest();
        request.getRequestBody().setStartDate(march11Morning());
        request.getRequestBody().setEndDate(march11Afternoon());
        CdrResponse response = cdrService.getCallsByCalledAndPeriod(request);
        verify(repo,times(1)).findByCalledPartyAndStartTimeAfterAndEndTimeBefore(any(), any(), any());
        assertEquals(CdrResponseType.SUCCESS, response.getCdrResponseType());
    }

    @Test
    public void getCalledByPeriodMissingDates() {
        CdrRequest request = createListByCalledPeriodRequest();
        CdrResponse response = cdrService.getCallsByCalledAndPeriod(request);
        verify(repo,times(0)).findByStartTimeAfterAndEndTimeBefore(any(), any());
        assertEquals(CdrResponseType.INCORRECT_REQUEST, response.getCdrResponseType());
    }


}