### Description

JAVA REST API for importing CDR records and querying imported data.
Technologies used:
- Spring Boot framework
- H2 in memory database
- JUnit and Mockito for testing

### Usage
Server listens at port 8080.
Services:
 - POST /api/upload
    - Uploads a CSV file with call data
    - File example:
```console
ID,ACCOUNT,DESTINATION,STARTDATE,ENDDATE,STATUS,COSTPERMINUTE
2a3ca8ee-3c02-4ae9-9166-141601ae64eb,+37069123456,+37069794444,1594150929,1594150987,success,0.05
```
	

- POST /api/allRecords
     - Lists all records in database
   - Request format is common for all non-file upload requests. Different functions require different fields filled; these field are specified for every function. 
	Request example:
    ````json
   {
  "apiVersion": "1.0",
  "cdrRequestType": "ALL_CALLS",
  "requestBody": {
       "calledParty": "string",
       "callingParty": "string",
       "endDate": "2020-07-31T14:39:29.210Z",
       "startDate": "2020-07-31T14:39:29.210Z",
       "status": "SUCCESS"
     }
   }
   ````

   - Response format (common for all non-file related functions):
    ````json
    {
  "cdrResponseBody": {
    "count": 0,
    "recordList": [
      {
        "calledParty": "string",
        "callingParty": "string",
        "cost": "string",
        "endTime": "2020-07-31T15:19:24.130Z",
        "recordId": "string",
        "startTime": "2020-07-31T15:19:24.130Z",
        "status": "SUCCESS"
      },
     {
        "calledParty": "string",
        "callingParty": "string",
        "cost": "string",
        "endTime": "2020-07-31T15:19:24.130Z",
        "recordId": "string",
        "startTime": "2020-07-31T15:19:24.130Z",
        "status": "SUCCESS"
      }
    ]
  },
  "cdrResponseType": "SUCCESS",
  "message": "string"
    }

 - POST /api/getCallsByCalled
    - Lists all calls received by particular user
    - Parameters:
        - user - phone number of the user
        - cdrRequestType must be set to CALLS_BY_CALLED_USER
 - POST /api/getCallsByCalledAndPeriod
    - Lists all calls received by particular user during specified period of time
    - Parameters:
        - user - phone number of the user
        - cdrRequestType must be set to CALLS_BY_CALLED_PERIOD
 - POST /api/getCallsByCaller
    - Lists all calls made by particular user
    - Parameters:
        - user - phone number of the user
        - cdrRequestType must be set to CALL_BY_USER
 - POST /api/getCallsByCallerAndPeriod
    - Lists all calls made by particular user during specified time period
    - Parameters:
        - user - phone number of the user
	- startDate - start date of time period
	- endDate - end date of time period
        - cdrRequestType must be set to CALL_BY_USER_PERIOD
 - POST /api/getCallsByPeriod
    - Lists all calls during time interval (all users)
    - Parameters:
	- startDate - start date of time period
	- endDate - end date of time period
        - cdrRequestType must be set to CALL_BY_PERIOD
 - POST /api/getCallsByStatus
    - Lists all calls with specified status
    - Parameter:
        - status - valid values are 'SUCCESS','FAIL'
        - cdrRequestType must be set to CALL_BY_STATUS
 - POST /api/getCountByCallerAndPeriod
    - Returns amount of calls made by specified user during time period
    - Parameter:
        - user - phone number of the user
	- startDate - start date of time period
	- endDate - end date of time period
        - cdrRequestType must be set to COUNT_BY_CALLER_PERIOD
                                                    

### Launch
To start please clone or unzip repository and run the maven task:

```console
git clone https://gitlab.com/eimantas1/cdrparser.git
cd cdrparser
mvn clean install spring-boot:run
```

Swagger UI can be used for testing:

http://localhost:8080/swagger-ui.html

H2 database can be queried at:

http://localhost:8080/h2-console

### Assumptions
   - Only phone call information is found in CSV files (no SMS, no Internet connection information)
   - CSV files are small enough to be processed synchronously


### Enhancements
   - All search functions have to support pagination for performance reasons
   - CSV file parsing might need asynchronous processing in background
   - H2 in-memory database suits for temporary use only; however in can be easily upgraded to H2-in-file database or MySQL
   - Security is lacking completely; authentication and IP based restrictions are needed.
